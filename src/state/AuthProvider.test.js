import React from 'react'
import ReactDOM from 'react-dom'
import { AuthProvider } from './Auth'

const noop = () => {}

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<AuthProvider updatelocalStorageData={noop} />, div)
  ReactDOM.unmountComponentAtNode(div)
})
