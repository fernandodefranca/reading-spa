import React, { useState, useEffect } from 'react'

const initialState = {
  isLogged: false,
  inProgress: false,
  failed: false,
  authData: {},
  userData: {},
}

const AuthContext = React.createContext([{}, () => {}])

const AuthProvider = (props) => {
  const { children, localStorageData, updatelocalStorageData } = props
  const [state, setState] = useState({ ...initialState, ...localStorageData })
  const { isLogged } = state

  useEffect(() => {
    updatelocalStorageData(state)
  }, [isLogged])

  return <AuthContext.Provider value={[state, setState]}>{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider, initialState }
