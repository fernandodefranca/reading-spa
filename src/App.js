import React from 'react'

import ThemeProvider from './components/ThemeProvider'
import NormalizeCSS from './components/NormalizeCSS'
import AppRouter from './components/AppRouter'
import LocalStorage from './components/LocalStorage'
import { AuthProvider } from './state/Auth'

function App() {
  return (
    <div className="App">
      <NormalizeCSS />
      <ThemeProvider>
        <LocalStorage>
          <AuthProvider>
            <AppRouter />
          </AuthProvider>
        </LocalStorage>
      </ThemeProvider>
    </div>
  )
}

export default App
