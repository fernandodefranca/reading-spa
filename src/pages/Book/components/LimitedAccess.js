import React from 'react'
import styled from 'styled-components'
import Button from '../../../components/Button'

const LimitedWrapper = styled.div`
  max-height: 300px;
  overflow: hidden;
  position: relative;
`

const Overlay = styled.div`
  background-color: #0003;
  position: absolute;
  width: 100%;
  height: 100%;
  background: linear-gradient(to bottom, rgba(30, 87, 153, 0) 55%, rgba(255, 255, 255, 1) 100%);
`

const Centered = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 196px;
`

export default function LimitedAccess({ children }) {
  return (
    <LimitedWrapper>
      <Overlay>
        <Centered className="Centered">
          <Button>Subscribe to read</Button>
        </Centered>
      </Overlay>
      {children}
    </LimitedWrapper>
  )
}
