import React from 'react'
import renderer from 'react-test-renderer'
import Component from './BookView'
import ThemeProvider from '../../components/ThemeProvider'

it('Renders correctly with given props', () => {
  const props = {
    content: {
      title: 'My title',
      content: 'My content',
    },
    isSubscriber: true,
  }
  const tree = renderer
    .create(
      <ThemeProvider>
        <Component {...props} />
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with given alternate props', () => {
  const props = {
    content: {
      title: 'My title',
      content: 'My content',
    },
    isSubscriber: false,
  }
  const tree = renderer
    .create(
      <ThemeProvider>
        <Component {...props} />
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
