import React, { useState, useEffect } from 'react'
import BooksService from '../../services/BooksService'
import useAuth from '../../hooks/useAuth'
import BookView from './BookView'

const initialState = {
  isFetching: false,
  content: null,
}

export default function Book(props) {
  const { id: bookId } = props.match.params
  const [state, setState] = useState({ ...initialState })
  const {
    userData: { accessType },
  } = useAuth()
  const isSubscriber = accessType === 'premium'

  const fetchContent = async (_bookId) => {
    setState({
      isFetching: true,
      content: null,
    })

    try {
      const { status, data } = await BooksService.fetchBook(_bookId)
      const success = status === 200 && !!data.content

      if (!success) {
        throw new Error('Could not load book data.')
      }

      setState({
        isFetching: false,
        content: data,
      })
    } catch (err) {
      setState({
        isFetching: false,
        content: null,
      })
    }
  }

  useEffect(() => {
    fetchContent(bookId)
  }, bookId)

  const { isFetching, content } = state

  return <BookView isFetching={isFetching} content={content} isSubscriber={isSubscriber} />
}
