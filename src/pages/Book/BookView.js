import React from 'react'
import styled from 'styled-components'
import Spinner from '../../components/Spinner'
import LimitedAccess from './components/LimitedAccess'

const Content = styled.div`
  line-height: 32px;
`

const UnlimitedAccess = styled.div``

export default function BookView(props) {
  const { isFetching, content, isSubscriber } = props

  if (isFetching) return <Spinner />

  const isContentValid = !!content && !!content.title && !!content.content

  if (!isContentValid) return null

  const Wrapper = isSubscriber ? UnlimitedAccess : LimitedAccess

  return (
    <Wrapper>
      <h1>{content.title}</h1>
      <Content>{content.content}</Content>
    </Wrapper>
  )
}
