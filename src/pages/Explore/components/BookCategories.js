import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import Spinner from '../../../components/Spinner'

const Wrapper = styled.div`
  margin-top: 16px;
  width: 160px;
`

const Category = styled(Link)`
  font-size: 18px;
  display: block;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.brandBlue};
`

function BookCategories({ categories }) {
  if (!categories) return null
  const { isFetching, content } = categories

  if (isFetching) return <Spinner />

  return (
    <Wrapper>
      {content &&
        content.map(({ id, title }) => {
          return (
            <Category key={id} to={`/explore/${id}`}>
              {title}
            </Category>
          )
        })}
    </Wrapper>
  )
}

export default React.memo(BookCategories)
