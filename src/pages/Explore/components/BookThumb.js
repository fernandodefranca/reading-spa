import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const ThumbImg = styled.div`
  width: 170px;
  height: 170px;
  background-image: url(${({ src }) => src});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`

const Thumb = styled(Link)`
  width: 170px;
  border: 1px solid #333;
  background-color: ${({ theme }) => theme.colors.grayLight};
  font-size: 16px;
  text-align: center;
  text-decoration: none;
  display: block;
  margin: 0 16px 16px 0;
  color: ${({ theme }) => theme.colors.brandBlue};
`

const ThumbTitle = styled.div`
  padding: 16px;
`

function BookThumb({ to, imgSrc, title }) {
  return (
    <Thumb to={to}>
      <ThumbImg src={imgSrc} alt={title} />
      <ThumbTitle>{title}</ThumbTitle>
    </Thumb>
  )
}

export default React.memo(BookThumb)
