import React from 'react'
import styled from 'styled-components'
import Spinner from '../../../components/Spinner'
import BookThumb from './BookThumb'

const Layout = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 16px 0 16px 16px;
`

function BookList({ books }) {
  if (!books) return null
  const { isFetching, content } = books

  if (isFetching) return <Spinner />

  return (
    <div>
      <Layout>
        {content &&
          content.map(({ id, image_url, title }) => {
            return <BookThumb to={`/book/${id}`} imgSrc={image_url} title={title} key={id} />
          })}
      </Layout>
    </div>
  )
}

export default React.memo(BookList)
