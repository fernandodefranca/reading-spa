import React from 'react'
import styled from 'styled-components'

import BookCategories from './components/BookCategories'
import BookList from './components/BookList'

const Wrapper = styled.div`
  display: flex;
`

export default function ExploreView(props) {
  const { categories, books } = props
  return (
    <Wrapper>
      <BookCategories categories={categories} />
      <BookList books={books} />
    </Wrapper>
  )
}
