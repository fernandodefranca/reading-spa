import React from 'react'
import BooksService from '../../services/BooksService'
import ExploreView from './ExploreView'

export default class Explore extends React.Component {
  state = {
    categories: {
      isFetching: false,
      content: null,
    },
    books: {
      isFetching: false,
      content: null,
    },
  }

  componentDidMount() {
    const categoryId = this.props.match.params.id
    this.fetchContent(categoryId)
  }

  fetchContent = (categoryId) => {
    this.fetchCategories()

    if (categoryId) {
      this.fetchCategoryBooks(categoryId)
    } else {
      this.fetchAllBooks()
    }
  }

  fetchCategories = async () => {
    this.setState({
      categories: {
        isFetching: true,
        content: null,
      },
      books: {
        isFetching: true,
        content: null,
      },
    })

    try {
      const { status, data } = await BooksService.fetchCategories()
      const success = status === 200 && data.categories

      if (!success) {
        throw new Error('Could not load category data.')
      }

      this.setState({
        categories: {
          isFetching: false,
          content: data.categories,
        },
      })
    } catch {
      this.setState({
        categories: {
          isFetching: false,
          content: null,
        },
      })
    }
  }

  fetchAllBooks = async () => {
    try {
      const { status, data } = await BooksService.fetchAllBooks()
      const success = status === 200 && data.books

      if (!success) {
        throw new Error('Could not load book data.')
      }

      this.setState({
        books: {
          isFetching: false,
          content: data.books,
        },
      })
    } catch (err) {
      this.setState({
        books: {
          isFetching: false,
          content: null,
        },
      })
    }
  }

  fetchCategoryBooks = async (categoryId) => {
    this.setState({
      allBooks: {
        isFetching: true,
        content: null,
      },
      books: {
        isFetching: true,
        content: null,
      },
    })

    try {
      const allBooks = await BooksService.fetchAllBooks()
      const allBooksSuccess = allBooks.status === 200 && allBooks.data.books

      const categoriesBooks = await BooksService.fetchCategoriesBooks(categoryId)
      const categoriesBooksSuccess = categoriesBooks.status === 200 && categoriesBooks.data.book_ids

      const success = allBooksSuccess && categoriesBooksSuccess

      if (!success) {
        throw new Error('Could not load all book data.')
      }

      const booksIds = categoriesBooks.data.book_ids
      const books = allBooks.data.books.filter((book) => {
        return booksIds.includes(book.id)
      })

      this.setState({
        allBooks: {
          isFetching: false,
          content: allBooks.data.books,
        },
        books: {
          isFetching: false,
          content: books,
        },
      })
    } catch (err) {
      this.setState({
        allBooks: {
          isFetching: false,
          content: null,
        },
        books: {
          isFetching: false,
          content: null,
        },
      })
    }
  }

  render() {
    const { categories, books } = this.state

    return (
      <div>
        <ExploreView categories={categories} books={books} />
      </div>
    )
  }
}
