import React from 'react'
import styled from 'styled-components'
import { Redirect } from 'react-router-dom'

import LoginButton from '../../components/Button'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 400px;
`

export default function HomeView({ isLogged, inProgress, failed, handleLogin }) {
  if (!isLogged) {
    return (
      <Wrapper>
        <LoginButton disabled={inProgress} type="button" onClick={handleLogin}>
          {inProgress ? 'Please, wait' : 'Log in'}
        </LoginButton>
        {failed && <p>Login failed</p>}
      </Wrapper>
    )
  }

  return <Redirect to="/explore" />
}
