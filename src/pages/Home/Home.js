import React from 'react'
import useAuth from '../../hooks/useAuth'
import View from './HomeView'

export default function Home() {
  const { isLogged, inProgress, failed, login, logout } = useAuth()

  const mockedLogin = () => {
    return login('MOCK_USER', 'MOCK_PASS')
  }

  return (
    <View
      isLogged={isLogged}
      inProgress={inProgress}
      failed={failed}
      handleLogin={mockedLogin}
      handleLogout={logout}
    />
  )
}
