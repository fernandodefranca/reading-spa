import React from 'react'
import renderer from 'react-test-renderer'
import { BrowserRouter as Router } from 'react-router-dom'
import Component from './HomeView'
import ThemeProvider from '../../components/ThemeProvider'

const handleLogin = () => {}

it('Renders correctly with given props', () => {
  const props = {
    isLogged: false,
    inProgress: false,
    failed: false,
    handleLogin,
  }
  const tree = renderer
    .create(
      <Router>
        <ThemeProvider>
          <Component {...props} />
        </ThemeProvider>
      </Router>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "inProgress" prop', () => {
  const props = {
    isLogged: false,
    inProgress: true,
    failed: false,
    handleLogin,
  }
  const tree = renderer
    .create(
      <Router>
        <ThemeProvider>
          <Component {...props} />
        </ThemeProvider>
      </Router>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "failed" prop', () => {
  const props = {
    isLogged: false,
    inProgress: false,
    failed: true,
    handleLogin,
  }
  const tree = renderer
    .create(
      <Router>
        <ThemeProvider>
          <Component {...props} />
        </ThemeProvider>
      </Router>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with "isLogged" prop', () => {
  const props = {
    isLogged: true,
    inProgress: false,
    failed: false,
    handleLogin,
  }
  const tree = renderer
    .create(
      <Router>
        <ThemeProvider>
          <Component {...props} />
        </ThemeProvider>
      </Router>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
