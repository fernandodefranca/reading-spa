export default {
  colors: {
    grayLight: '#f1f6f4',
    brandGreen: '#3cdd82',
    brandBlue: '#032f53',
  },
  dimensions: {
    borderRadius: 4,
  },
}
