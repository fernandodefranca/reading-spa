import { useContext } from 'react'
import { AuthContext, initialState } from '../state/Auth'
import AuthService from '../services/AuthService'

const useAuth = () => {
  const [state, setState] = useContext(AuthContext)

  async function getUserData() {
    try {
      const { status, data } = await AuthService.me()
      const success = status === 200

      if (success) {
        return {
          accessType: data.access_type,
        }
      }

      return {
        accessType: null,
      }
    } catch (err) {
      return {
        accessType: null,
      }
    }
  }

  async function login(user, password) {
    setState((currentState) => ({ ...currentState, inProgress: true, failed: false }))
    try {
      const { status, data } = await AuthService.auth(user, password)
      const success = status === 200 && data.status === 'success' && !!data.user_id
      let newState

      if (success) {
        const userData = await getUserData()

        newState = {
          inProgress: false,
          failed: false,
          isLogged: true,
          authData: data,
          userData,
        }

        getUserData()
      } else {
        newState = {
          ...initialState,
          failed: true,
        }
      }

      setState((currentState) => ({
        ...currentState,
        ...newState,
      }))
    } catch {
      this.logout()
    }
  }

  function logout() {
    setState(() => ({ ...initialState }))
  }

  return {
    ...state,
    login,
    logout,
  }
}

export default useAuth
