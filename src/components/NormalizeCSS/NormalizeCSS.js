import { createGlobalStyle } from 'styled-components'
import styledNormalize from 'styled-normalize'

const NormalizeCSS = createGlobalStyle`
  ${styledNormalize}
  html, body {
    padding: 0;
    margin: 0;
    background-color: #fff;
    font-family: Helvetica,Arial,sans-serif;
    overflow: scroll;
  }
  a {
    text-decoration: none;
  }
`

export default NormalizeCSS
