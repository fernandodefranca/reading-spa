import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import useAuth from '../../hooks/useAuth'
import NavBarLink from '../NavBar/NavBarLink'

import NavBar from '../NavBar'
import PageWrapper from '../PageWrapper'
import Home from '../../pages/Home'
import Explore from '../../pages/Explore'
import Book from '../../pages/Book'
import Logout from '../../pages/Logout'
import Error404 from '../../pages/Error404'

export default function AppRouter() {
  const { isLogged } = useAuth()

  return (
    <Router>
      <NavBar>
        {isLogged ? (
          <React.Fragment>
            <NavBarLink to="/explore">Discover books</NavBarLink>
            <NavBarLink to="/logout">Logout</NavBarLink>
          </React.Fragment>
        ) : (
          <NavBarLink to="/">Log in</NavBarLink>
        )}
      </NavBar>
      <PageWrapper>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route
            path="/explore/:id"
            render={(props) => <Explore {...props} key={props.match.params.id} />}
          />
          <Route path="/explore" component={Explore} />
          <Route path="/book/:id" component={Book} />
          <Route path="/logout" component={Logout} />
          <Route component={Error404} />
        </Switch>
      </PageWrapper>
    </Router>
  )
}
