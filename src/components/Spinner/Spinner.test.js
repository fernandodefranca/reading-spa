import React from 'react'
import Component from '../Spinner'
import renderer from 'react-test-renderer'

it('Renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with size', () => {
  const tree = renderer.create(<Component size={24} />).toJSON()
  expect(tree).toMatchSnapshot()
})
