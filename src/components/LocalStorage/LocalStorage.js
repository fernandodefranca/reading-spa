import React from 'react'

const KEY = 'READING_APP_STORAGE'

export default class LocalStorage extends React.Component {
  state = {
    isLoaded: false,
    data: null,
  }

  componentDidMount() {
    let data = {}

    try {
      const storedString = localStorage.getItem(KEY)
      data = JSON.parse(storedString)
    } catch (err) {
      // eslint-disable-next-line
      console.error(err)
    }

    this.setState({ isLoaded: true, data })
  }

  updatelocalStorageData = (data) => {
    try {
      // Do not set state as it will trigger an undesired render
      localStorage.setItem(KEY, JSON.stringify(data))
    } catch (err) {
      // eslint-disable-next-line
      console.error(err)
    }
  }

  render() {
    const { props, state, updatelocalStorageData } = this
    const { children } = props
    const { isLoaded, data } = state

    if (!isLoaded) return null

    const enhancedChildren = React.Children.map(children, (child) => {
      return React.cloneElement(child, {
        localStorageData: data,
        updatelocalStorageData,
      })
    })
    return enhancedChildren
  }
}
