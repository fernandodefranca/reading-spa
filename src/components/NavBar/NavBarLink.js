import styled from 'styled-components'
import { Link } from 'react-router-dom'

const NavBarLink = styled(Link)`
  font-size: 18px;
  display: block;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.brandBlue};
  margin-left: 16px;
`

export default NavBarLink
