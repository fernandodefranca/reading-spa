import React from 'react'
import renderer from 'react-test-renderer'
import Component from './'
import ThemeProvider from '../ThemeProvider'
import { BrowserRouter as Router } from 'react-router-dom'

it('Renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider>
        <Router>
          <Component />
        </Router>
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with given children', () => {
  const tree = renderer
    .create(
      <ThemeProvider>
        <Router>
          <Component>My label</Component>
        </Router>
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
