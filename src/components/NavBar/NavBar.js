import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import logoImage from '../../assets/blinkist-logo.png'

const Wrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.grayLight};
  padding: 28px 16px;
  display: flex;
  justify-content: center;
`

const Nav = styled.div`
  width: 100%;
  max-width: 944px;
  display: flex;
`

const Logo = styled(Link)`
  width: 144px;
  height: 37px;
  background-image: url(${logoImage});
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
`

const NavLinks = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: flex-end;
`

export default function NavBar({ children }) {
  return (
    <Wrapper>
      <Nav>
        <Logo to="/" />
        <NavLinks>{children}</NavLinks>
      </Nav>
    </Wrapper>
  )
}
