import styled from 'styled-components'

const Button = styled.button`
  color: ${({ theme }) => theme.colors.brandBlue};
  font-size: 24px;
  font-weight: bold;
  min-width: 280px;
  padding: 20px 40px;
  background-color: ${({ theme }) => theme.colors.brandGreen};
  border-radius: ${({ theme }) => theme.dimensions.borderRadius}px;
  border: none;
  opacity: ${({ disabled }) => (disabled ? 0.4 : 1)};
`

export default Button
