import React from 'react'
import renderer from 'react-test-renderer'
import Component from './'
import ThemeProvider from '../ThemeProvider'

it('Renders correctly', () => {
  const tree = renderer
    .create(
      <ThemeProvider>
        <Component />
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with given children', () => {
  const tree = renderer
    .create(
      <ThemeProvider>
        <Component>My label</Component>
      </ThemeProvider>
    )
    .toJSON()
  expect(tree).toMatchSnapshot()
})
