import React from 'react'
import styled from 'styled-components'

const OuterWrapper = styled.div`
  padding: 0px 16px;
  display: flex;
  justify-content: center;
`

const InnerWrapper = styled.div`
  width: 100%;
  max-width: 944px;
`

export default function PageWrapper({ children }) {
  return (
    <OuterWrapper>
      <InnerWrapper>{children}</InnerWrapper>
    </OuterWrapper>
  )
}
