import React from 'react'
import renderer from 'react-test-renderer'
import Component from './'

it('Renders correctly', () => {
  const tree = renderer.create(<Component />).toJSON()
  expect(tree).toMatchSnapshot()
})

it('Renders correctly with given children', () => {
  const tree = renderer.create(<Component>My label</Component>).toJSON()
  expect(tree).toMatchSnapshot()
})
