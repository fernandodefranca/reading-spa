import axios from 'axios'
import config from './config'

const { baseURL } = config

export default class AuthService {
  static auth(user, password) {
    if (!user || !password)
      throw new Error('Auth.auth failed: method requires "user" and "password" parameters.')

    const url = `${baseURL}/auth`
    const payload = { user, password }
    return axios.post(url, payload)
  }

  static me() {
    const url = `${baseURL}/me`
    return axios.get(url)
  }
}
