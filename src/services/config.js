const env = process.env.NODE_ENV
const isDevelopment = env === 'development'
const productionURL = 'https://ancient-springs-73658.herokuapp.com'
const baseURL = isDevelopment ? '' : productionURL

export default {
  baseURL,
}
