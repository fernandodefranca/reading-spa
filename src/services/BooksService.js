import axios from 'axios'
import config from './config'

const { baseURL } = config

export default class BooksService {
  static fetchCategories() {
    const url = `${baseURL}/categories`
    return axios.get(url)
  }

  static fetchCategoriesBooks(id) {
    if (!id)
      throw new Error(`BooksService.fetchCategoriesBooks failed: "id" parameter is required.`)

    const url = `${baseURL}/categories/${id}`
    return axios.get(url)
  }

  static fetchAllBooks() {
    const url = `${baseURL}/books`
    return axios.get(url)
  }

  static fetchBook(id) {
    if (!id) throw new Error(`BooksService.fetchBook failed: "id" parameter is required.`)

    const url = `${baseURL}/books/${id}`
    return axios.get(url)
  }
}
