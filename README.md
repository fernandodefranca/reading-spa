# Reading Single Page Application

## Common tasks (testing, running)

- `yarn start` will start the app with the dev server.
- `yarn test` will run all tests.
- `build-and-serve` will generate a production build and start a server on port 5000 (if available). ⚠️ Beware of API CORS limitation.

## Observations

- Due to CORS issues with preflight running locally will result in all requests failing with 404 status. Just run `yarn start` and CRA's proxy will take care of it.
- Development environment: node v10.16.0

## Possible improvements

- Use .env files for baseURL on different stages (development, staging, production)
- Tests beyond simple snapshots
- Proper API response handling (timeout, retry, errors)
- Prop-types were skipped due to time limitation
- Keep all book data in a dedicated context and avoid unnecessary requests
- Limit the number of results at the books list component (/explore). Actually is infinite 🤨
- Responsivity, especially for the books list component
- Category menu - represent the currently active category
- Absolute paths - Jest/Node rejects absolute paths and it requires to be setup properly

## Bonus

- Page reloads and session persistence is already handled by storing user data at the browser local storage 😉
- Properly handling an audio player feature would require:
    - communicate between tabs/windows
    - ensure that only one tab is playing audio
    - stop players in other tabs/windows when a new audio player is started
- For a search feature, I would use the same abstraction of keeping state within the path/router parameter and forward it to the API requests
- For complex flows like checkout or onboarding I would use some Finite-state machine based solution as it brings reliability, improves the readability of the flow and it's evolution/refactoring become easier.
